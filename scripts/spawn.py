#!/usr/bin/env python
import rospy
import shutil
import random
from gazebo_msgs.srv import SpawnModel
from geometry_msgs.msg import Pose


 
def set_cube(default_size, color):

#    shutil.copyfile('/home/robot/catkin_ws_second/src/task_1/urdf/box_dummy_original.urdf', '/home/robot/catkin_ws_second/src/task_1/urdf/box_dummy_copy.urdf')

    with open('/home/robot/catkin_ws_second/src/task_1/urdf/box_dummy_copy.urdf', 'rt') as file:
        replace_size = file.read().replace('{default_size}', default_size)
        replace_color = replace_size.replace('Gazebo/Grey', color)
    
    with open('/home/robot/catkin_ws_second/src/task_1/urdf/box_dummy_copy.urdf', 'wt') as file:
        file.write(replace_color)
        
def restore_cube_file(default_size, color):
    with open('/home/robot/catkin_ws_second/src/task_1/urdf/box_dummy_copy.urdf', 'rt') as file:
        replace_size = file.read().replace(default_size, '{default_size}')
        replace_color = replace_size.replace(color, 'Gazebo/Grey')
    
    with open('/home/robot/catkin_ws_second/src/task_1/urdf/box_dummy_copy.urdf', 'wt') as file:
        file.write(replace_color)

        
def spawn(client, boxname, size, color, position):
    
    set_cube(str(size), color)

    model_pose = Pose()
    model_pose.position.x = position['x']
    model_pose.position.y = position['y']
    model_pose.position.z = position['z']
    model_pose.orientation.x = 0.0
    model_pose.orientation.y = 0.0
    model_pose.orientation.z = 0.0
    model_pose.orientation.w = 1.0
    
    client(
        model_name=boxname,
        model_xml=open('/home/robot/catkin_ws_second/src/task_1/urdf/box_dummy_copy.urdf', 'rt').read(),
        robot_namespace='/foo',
        initial_pose=model_pose,
        reference_frame='world'
    )
    

def main():       
    
    rospy.init_node('spawner')
    rospy.wait_for_service('/gazebo/spawn_urdf_model')
    spawn_model_client = rospy.ServiceProxy('/gazebo/spawn_urdf_model', SpawnModel)
    
    colors = ('Gazebo/White', 'Gazebo/Black', 'Gazebo/Red', 'Green', 'Gazebo/Blue', 'Gazebo/Yellow', 'Gazebo/Purple')
    random.seed()

    for i in range(19):
        size = round(random.uniform(0.3, 1.4), 2)
        color = random.choice(colors)
        spawn(spawn_model_client, f'box_{i}', size, color, {'x': random.randint(-10, 10), 'y': random.randint(-10, 10), 'z': 4})
        restore_cube_file(str(size), color)
        
   

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
